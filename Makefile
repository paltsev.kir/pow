.PHONY: server
server:
	@mkdir -p build
	@go build -o build/server ./server/

.PHONY: client
client:
	@mkdir -p build
	@go build -o build/client ./client/

.PHONY: pack
pack:
	tar --exclude='./.idea' -zcvf kirill_solution.tar.gz .
