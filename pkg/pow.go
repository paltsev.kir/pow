package pkg

import (
	"bytes"
	"crypto/sha256"
	"encoding/binary"
)

func CheckPOW(complexity byte, nonce uint64, data []byte) bool {
	prefix := make([]byte, complexity)
	nonceBytes := make([]byte, 8)

	binary.LittleEndian.PutUint64(nonceBytes, nonce)

	hash := sha256.Sum256(append(data, nonceBytes...))
	return bytes.HasPrefix(hash[:], prefix)
}
