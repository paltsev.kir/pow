package pkg

type proto byte

const (
	RequestNewQuote       proto = 1
	RequestSolveChallenge proto = 3
)

const (
	QuoteLenBytes = 8
)

func (p proto) Bytes(bytes ...byte) []byte {
	return append([]byte{byte(p)}, bytes...)
}
