package internal

import (
	"fmt"
	"github.com/stretchr/testify/require"
	"testing"
)

func TestQuotes(t *testing.T) {
	t.Skip("manual")

	quotes, err := NewQuotes(``)
	require.NoError(t, err)

	for i := 0; i < len(quotes); i++ {
		quote := quotes.RandomQuote()
		fmt.Printf("%d: %v\n", i, quote)
	}
	fmt.Printf("Quotes:\n%v\n", len(quotes))
}
