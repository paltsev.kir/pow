package internal

import (
	"crypto/rand"
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"io"
	"net"
	"pow/pkg"
)

const complexity byte = 2

type Server struct {
	addr   string
	log    *logrus.Logger
	quotes quotes
}

func NewServer(addr string, q quotes, log *logrus.Logger) *Server {
	return &Server{addr: addr, quotes: q, log: log}
}

func (s *Server) Run() error {
	listener, err := net.Listen("tcp", s.addr)
	if err != nil {
		return err
	}
	defer listener.Close()

	for {
		conn, err := listener.Accept()
		if err != nil {
			s.log.Errorf("Error accepting connection: %v", err)
			continue
		}

		s.log.Debugf("Accepted connection from %v. Proceeding...", conn.RemoteAddr())
		go s.handleClient(conn)
	}
}

func (s *Server) handleClient(conn net.Conn) {
	defer conn.Close()

	for {
		buff := make([]byte, 10)
		_, err := io.ReadAtLeast(conn, buff, 1)
		if err != nil {
			if !errors.Is(err, io.EOF) {
				s.log.Errorf("Error during read connection %v: %v", conn.RemoteAddr(), err)
			}
			return
		}
		cmd := buff[0]

		if cmd == byte(pkg.RequestNewQuote) {
			err = s.handleQuoteRequest(conn)
			if err != nil {
				if !errors.Is(err, io.EOF) {
					s.log.Errorf("Error handling quote request: %v", err)
				}
				return
			}
		} else {
			s.log.Errorf("Unknown command %b. Exiting", cmd)
			return
		}
	}
}

func (s *Server) handleQuoteRequest(conn net.Conn) error {
	const numOfDataBytes byte = 8

	data := make([]byte, numOfDataBytes)
	_, err := rand.Read(data)
	if err != nil {
		return err
	}

	buff := append([]byte{complexity, numOfDataBytes}, data...)
	s.log.Debugf("Challenging client %v", conn.RemoteAddr())
	_, err = conn.Write(buff)
	if err != nil {
		return err
	}

	s.log.Debug("Waiting for challenge response")
	_, err = io.ReadAtLeast(conn, buff, 9)
	if err != nil {
		return err
	}
	nonce := binary.LittleEndian.Uint64(buff[1:])

	if !pkg.CheckPOW(complexity, nonce, data) {
		s.log.Infof("Client %v has solved challenge", conn.RemoteAddr())
	} else {
		return fmt.Errorf("client %v failed to solve challenge", conn.RemoteAddr())
	}

	s.log.Debugf("Generating random quote for client %v", conn.RemoteAddr())

	quote := s.quotes.RandomQuote()
	quoteLen := len(quote)

	s.log.Debugf("Quote len is %d and it's %q", quoteLen, quote)

	writeBuff := make([]byte, 8+quoteLen)
	binary.LittleEndian.PutUint64(writeBuff, uint64(quoteLen))
	copy(writeBuff[8:], quote)

	_, err = conn.Write(writeBuff)
	return err
}
