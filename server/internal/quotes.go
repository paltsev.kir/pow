package internal

import (
	"math/rand"
	"os"
	"strings"
)

type quotes []string

func NewQuotes(filename string) (quotes, error) {
	buff, err := os.ReadFile(filename)
	if err != nil {
		return nil, err
	}

	str := strings.Trim(string(buff), "\n")
	str = strings.ReplaceAll(str, "\r", "")
	paragraphs := strings.Split(str, "\n")

	return paragraphs, nil
}

func (q quotes) RandomQuote() string {
	randomIndex := rand.Intn(len(q))
	return q[randomIndex]
}
