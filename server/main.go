package main

import (
	"github.com/sirupsen/logrus"
	"os"
	"pow/server/internal"
)

func main() {
	log := logrus.New()
	log.Level = logrus.DebugLevel

	if len(os.Args) < 3 {
		log.Fatal("Usage: server [listen_address] [file with quotes]")
	}
	addr := os.Args[1]
	filename := os.Args[2]

	quotes, err := internal.NewQuotes(filename)
	if err != nil {
		log.Fatalf("Can't create quotes: %v", err)
	}
	log.Infof("Number of quotes in %q is %d", filename, len(quotes))

	server := internal.NewServer(addr, quotes, log)
	if err := server.Run(); err != nil {
		log.Errorf("Error during server run: %v", err)
	}
}
