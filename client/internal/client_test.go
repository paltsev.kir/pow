package internal

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/require"
	"math"
	"testing"
)

func Test_solveChallenge(t *testing.T) {
	c := client{
		maxIterations: math.MaxInt,
		log:           logrus.New(),
	}

	nonce, err := c.solveChallenge(1, []byte{'P', 'R', 'O', 'O', 'F'})
	require.NoError(t, err)

	fmt.Println("nonce is ", nonce)
}
