package internal

import (
	"encoding/binary"
	"errors"
	"fmt"
	"github.com/sirupsen/logrus"
	"io"
	"net"
	"pow/pkg"
	"strings"
)

type client struct {
	conn          net.Conn
	maxIterations int

	log *logrus.Logger
}

func NewClient(host string, port uint16, maxIterations int, log *logrus.Logger) (*client, error) {
	addr := fmt.Sprintf("%s:%d", host, port)
	conn, err := net.Dial("tcp", addr)
	if err != nil {
		return nil, err
	}

	return &client{conn, maxIterations, log}, nil
}

func (c *client) NewQuote() (string, error) {
	c.log.Debug("Requesting new quote from server")
	_, err := c.conn.Write(pkg.RequestNewQuote.Bytes())
	if err != nil {
		return "", err
	}

	complexity, data, err := c.readChallenge()
	if err != nil {
		return "", err
	}

	c.log.Debugf("Solving challenge complexity=%v dataLen=%d", complexity, len(data))
	nonce, err := c.solveChallenge(complexity, data)
	if err != nil {
		return "", err
	}

	c.log.Debugf("Sending solved challenge nonce=%v", nonce)
	nonceBytes := make([]byte, 8)
	binary.LittleEndian.PutUint64(nonceBytes, nonce)
	_, err = c.conn.Write(pkg.RequestSolveChallenge.Bytes(nonceBytes...))
	if err != nil {
		return "", err
	}

	return c.readQuote()
}

func (c *client) readQuote() (string, error) {
	buff := make([]byte, 1024)

	const quoteLenBytes = 8
	nR, err := io.ReadAtLeast(c.conn, buff, quoteLenBytes)
	if err != nil {
		return "", err
	}

	quoteLen := binary.LittleEndian.Uint64(buff)
	bytesLeft := quoteLen - uint64(nR-quoteLenBytes)

	c.log.Debugf("Reading quote quoteLen=%d bytesLeft=%d", quoteLen, bytesLeft)

	var str strings.Builder
	str.Write(buff[quoteLenBytes:nR])

	_, err = io.CopyN(&str, c.conn, int64(bytesLeft))
	if err != nil {
		return "", err
	}

	return str.String(), nil
}

func (c *client) readChallenge() (complexity byte, data []byte, err error) {
	buff := make([]byte, 1024)

	nR, err := io.ReadAtLeast(c.conn, buff, 2)
	if err != nil {
		return
	}

	complexity = buff[0]
	numOfChallengeBytes := buff[1]
	// how many bytes except for first byte (complexity) & second byte (number of challenge bytes) are left in network
	bytesToRead := numOfChallengeBytes - byte(nR-2)

	c.log.Debugf("Reading challenge data bytesToRead=%v, nR=%v, len(buff)=%v numOfChallengeBytes=%v", bytesToRead, nR, len(buff), numOfChallengeBytes)

	if bytesToRead > 0 {
		_, err = io.ReadAtLeast(c.conn, buff[nR:], int(bytesToRead))
		if err != nil {
			return
		}
	}
	data = buff[2:numOfChallengeBytes]

	return
}

// note that solveChallenge owns data slice
func (c *client) solveChallenge(complexity byte, data []byte) (nonce uint64, err error) {
	for i := 0; i < c.maxIterations; i++ {
		if pkg.CheckPOW(complexity, nonce, data) {
			return
		}

		nonce++
	}

	err = errors.New("max iterations reached, no solution found")
	return
}

func (c *client) Close() error {
	return c.conn.Close()
}
