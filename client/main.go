package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"math"
	"os"
	"pow/client/internal"
	"strconv"
)

func main() {
	log := logrus.New()
	log.Level = logrus.DebugLevel

	if len(os.Args) < 4 {
		log.Fatal("Usage: client [host] [port] [quotesN]")
	}

	host := os.Args[1]
	port, err := strconv.Atoi(os.Args[2])
	if err != nil {
		log.Fatalf("Error parsing port: %v", err)
	}
	quotesN, err := strconv.Atoi(os.Args[3])
	if err != nil {
		log.Fatalf("Error parsing quotes number argument: %v", err)
	}

	cl, err := internal.NewClient(host, uint16(port), math.MaxInt, log)
	if err != nil {
		log.Fatalf("Error creating client: %v", err)
	}
	defer cl.Close()

	for i := 0; i < quotesN; i++ {
		quote, err := cl.NewQuote()
		if err != nil {
			fmt.Printf("Error: %v\n", err)
		} else {
			fmt.Printf("New quote: '%s'\n", quote)
		}
	}
}
